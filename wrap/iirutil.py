"""Utility functions for IIR filters"""

from typing import Union
from collections.abc import Sequence
import functools
import numpy as np


_CplxArrayLike = Union[Sequence[complex], np.ndarray]


# extracted from iirutil.cc
# https://git.ligo.org/cds/software/gds-sigp/-/blob/main/src/SignalProcessing/IIRFilter/iirutil.cc

_epsilon = 1e-10
_epsilon_sqr = 1e-6


def _quadroots(a: float, b: float, c: float) -> tuple[complex, complex]:
    D = b * b - 4 * a * c
    if b < 0:
        q = -(b - np.emath.sqrt(D)) / 2
    else:
        q = -(b + np.emath.sqrt(D)) / 2
    return q / a, c / q


def _root_cmp(c1: complex, c2: complex, splane: bool = True) -> int:
    f0fs = 0
    if not splane:
        f0fs = 1
    if np.abs(np.imag(c1)) < _epsilon:
        if np.abs(np.imag(c2)) < _epsilon:
            # both real
            return -1 if np.abs(np.real(c1 - f0fs)) < np.abs(np.real(c2 - f0fs)) else 1
        else:
            # c1 real
            return 1
    else:
        if np.abs(np.imag(c2)) < _epsilon:
            # c2 real
            return -1
        else:
            # both complex
            m1 = np.abs(c1 - f0fs)
            m2 = np.abs(c2 - f0fs)
            i1 = np.abs(np.imag(c1))
            i2 = np.abs(np.imag(c2))
            if np.abs(m2 - m1) > _epsilon_sqr:
                return -1 if m1 < m2 else 1
            elif np.abs(i2 - i1) > _epsilon_sqr:
                return -1 if i1 < i2 else 1
            # positive imag part first
            else:
                return -1 if np.imag(c1) >= 0 else 1


def sorted_roots(roots: _CplxArrayLike, splane: bool = True) -> np.ndarray:
    """Returns a sorted list of roots in ascending order.

    Complex roots are grouped as complex conjugate pairs. If the roots
    are in the s-plane, they are ordered in increasing magnitude and
    increasing Q values. If the roots are in the z-plane, they are
    ordered in increasing distance from z=1 and increasing imaginary
    part. Real roots always come last.

    Args:
        roots: list of roots
        splane: True if in s-plane, False if in z-plane

    """
    # sort by magnitude and angle
    keyfunc = functools.cmp_to_key(functools.partial(_root_cmp, splane=splane))
    roots = sorted(roots, key=keyfunc)
    roots = np.asarray(roots, dtype=complex)
    # make sure that multiple identical roots are in complex conjugate pairs
    idx = np.nonzero(np.abs(np.imag(roots)) >= _epsilon)[0]  # index of complex roots
    if len(idx) > 0:
        # complex roots exist
        if len(idx) % 2 == 1:
            raise ValueError("array contains complex value with no matching conjugate")
        # pair them up
        for i in range(0, len(idx), 2):
            r_i = roots[idx[i]]
            # indices of matching conjugates
            conj_idx = np.nonzero(np.abs(roots - np.conj(r_i)) < _epsilon_sqr)[0]
            # skip those that were already processed
            conj_idx = conj_idx[conj_idx > i]
            if not len(conj_idx) > 0:
                raise ValueError(
                    "array contains complex value with no matching conjugate"
                )
            # swap conjugate into place
            if not conj_idx[0] == idx[i] + 1:
                roots[[idx[i] + 1, conj_idx[0]]] = roots[[conj_idx[0], idx[i] + 1]]
    return roots


def bilinear(fs: float, root: complex, prewarp: bool = True) -> tuple[complex, float]:
    """Performs the bilinear transform.

    The transform will take a an s-plane pole or zero (`s_p`) as the
    argument and return the z-plane root, `z_p`, and its gain. For a
    pole we have:

    `1/(s - s_p) \\mapsto g (1 + z^{-1}) / (1 - z_p z^{-1})`

    with

    `z_p = (2 f_s + s_p) / (2 f_s - s_p) ; g = 1/(2 f_s - s_p)`.

    The function returns the absolute value of the gain, `g`, if `s_p`
    is complex. Since complex poles must come in complex conjugate
    pairs, the overall gain will always be real.

    For a zero we have

    `s + s_z \\mapsto (1/g) (1 - z_z z^{-1}) / (1 + z^{-1})`

    where

    `z_z = (2 f_s + s_z) / (2 f_s - s_z) ; g = 1/(2 f_s - s_z)`.

    The function returns the absolute value of `g`, if `s_p` is
    complex.  There is always a root at the Nyquist frequency which is
    ignored in the return of this function.

    If prewarping is requested (default), the s-plane root is first
    scaled by `(2 f_s/|s_p|) \\tan(|s_p|/(2 f_s))`.  The returned gain
    is also multiplied by this factor.

    Args:
        fs: sample rate
        root: s-plane root
        prewarp: True for prewarping frequencies

    Returns:
        z-plane root, gain adjustment

    """
    # prewarp
    g = 1.0
    if prewarp:
        if np.abs(np.real(root)) > 2 * np.pi * fs / 2:
            # refuse to prewarp roots that exceed the Nyquist frequency
            # since it would push them across the unit circle
            raise ValueError(f"root {root} exceeds the Nyquist frequency ({fs/2})")
        mag = np.abs(root)
        if mag > 0:
            g = (2 * fs / mag) * np.tan(mag / (2 * fs))
            root *= g
    # transform
    denom = 1 / (2 * fs - root)
    root = (2 * fs + root) * denom
    return root, g * np.abs(denom)


def inverse_bilinear(
    fs: float, root: complex, unwarp: bool = True
) -> tuple[complex, float]:
    """Performs the inverse bilinear transform.

    The transform will take a z-plane root, `z_p`, as the argument,
    and return the s-plane pole or zero, `s_p`, and its gain. For a
    pole we have:

    `(1 + z^{-1}) / (1 - z_p z^{-1}) \\mapsto g (1/(s - s_p)`

    with

    `s_p = 2 f_s (z_p - 1) / (z_p + 1)`

    and

    `g = 4 f_s/(z_p + 1)`

    The function assumes an additional zero at the Nyquist frequency.
    It returns the absolute value of the gain, `g`, if `z_p` is
    complex.  Since complex poles must come in complex conjugate
    pairs, the overall gain will always be real. For a zero we have

    `(1 - z_p z^{-1}) / (1 + z^{-1}) \\mapsto (1/g) (s - s_z)`

    with

    `s_z = 2 fs (z_p - 1) / (z_p + 1)`

    and

    `g = 4 f_s/(z_p + 1)`

    Again, the function returns the absolute value of `g`, if `s_p` is
    complex.

    If unwarping is requested (default), the s-plane root is further
    scaled by `(2 f_s/|s_p|) \\arctan(|s_p|/(2 f_s))`.  The returned
    gain is also multiplied by this factor.

    Args:
        fs: sample rate
        root: z-plane root
        unwarp: True for prewarping frequencies

    Returns:
        s-plane root, gain adjustment

    """
    # inverse transform
    denom = 2 * fs / (root + 1)
    root = (root - 1) * denom
    # unwarp
    g = 1.0
    if unwarp:
        mag = np.abs(root)
        if mag > 0:
            g = (2 * fs / mag) * np.arctan(mag / (2 * fs))
            root *= g
    return root, 2 * g * np.abs(denom)


def s2z(
    fs: float,
    zeros: _CplxArrayLike,
    poles: _CplxArrayLike,
    gain: float,
    plane: str = "s",
    prewarp: bool = True,
) -> tuple[np.ndarray, np.ndarray, float]:
    """Transforms s-plane roots into z-plane roots.

    Poles and zeros are specifed in the s-plane using units of rad/s
    (set plane to 's') or units of Hz ('n'). In both cases, the real
    part of the roots are expected to be NEGATIVE. Alternatively, one
    can also specify normalized poles and zeros ('n'). Normalized
    poles and zeros are expected to have POSITIVE real parts, and
    their respective low and high frequency gains are set to 1 --
    unless they are located at 0 Hz. The default location is the
    s-plane.

    The returned number of poles and zeros are identical to the
    inputs.

    This function uses the bilinear transform, and ignores any z-plane
    roots at the Nyquist frequency that might be added by this
    transform.

    Args:
        fs: sample rate
        zeros: array of s-plane zeros
        poles: array of s-plane poles
        gain: gain
        plane: location where s-plane poles/zeros are specified
        prewarp: True for prewarping frequencies

    Returns:
        array of z-plane zeros, array of z-plane poles, gain

    """
    zeros = np.asarray(zeros, dtype=complex)
    poles = np.asarray(poles, dtype=complex)
    # plane conversions
    if plane == "n":
        f0 = np.abs(zeros)
        f0[f0 <= _epsilon] = 1
        zeros *= -2 * np.pi
        gain /= float(np.prod(2 * np.pi * f0))
        f0 = np.abs(poles)
        f0[f0 <= _epsilon] = 1
        poles *= -2 * np.pi
        gain *= float(np.prod(2 * np.pi * f0))
    elif plane == "f":
        zeros = 2 * np.pi * zeros
        poles = 2 * np.pi * poles
    elif plane != "s":
        raise ValueError(f"unsupported plane: {plane}")
    # need to make sure we have complex conjugate pairs
    zeros = sorted_roots(zeros)
    poles = sorted_roots(poles)
    # transform
    for i in range(len(zeros)):
        # bilinear
        zeros[i], g = bilinear(fs, zeros[i], prewarp=prewarp)
        gain /= g
    for i in range(len(poles)):
        # bilinear
        poles[i], g = bilinear(fs, poles[i], prewarp=prewarp)
        gain *= g
    zeros = sorted_roots(zeros, splane=False)
    poles = sorted_roots(poles, splane=False)
    return (zeros, poles, gain)


def z2s(
    fs: float,
    zeros: _CplxArrayLike,
    poles: _CplxArrayLike,
    gain: float,
    plane: str = "s",
    unwarp: bool = True,
) -> tuple[np.ndarray, np.ndarray, float]:
    """Transforms z-plane roots into s-plane roots.

    Poles and zeros are specified in the s-plane using units of rad/s
    (set plane to 's'), or units of Hz ('f'). In both cases, the real
    part of the roots are expected to be NEGATIVE. Alternatively, one
    can also specify normalized poles and zeros ('n'). Normalized
    poles and zeros are expected to have POSITIVE real parts, and
    their respective low and high frequency gains are set to 1 --
    unless they are located at 0 Hz. The default location is the
    s-plane.

    The returned number of poles and zeros are identical to the
    inputs.

    This function uses the inverse bilinear transform, and assumes any
    z-plane roots at the Nyquist frequency that might be required by
    this transform. Hence, the input list must not contain any poles
    or zeros at the Nyquist frequency.

    Args:
        fs: sample rate
        zeros: list of z-plane zeros
        poles: list of z-plane poles
        gain: gain
        plane: location where s-plane poles/zeros are specified
        unwarp: True for unwarping frequencies

    Returns:
        array of s-plane zeros, array of s-plane poles, gain

    """
    zeros = np.asarray(zeros, dtype=complex)
    poles = np.asarray(poles, dtype=complex)
    # zeros
    for i in range(len(zeros)):
        # inverse bilinear
        zeros[i], g = inverse_bilinear(fs, zeros[i], unwarp=unwarp)
        gain /= g
    # poles
    for i in range(len(poles)):
        # inverse bilinear
        poles[i], g = inverse_bilinear(fs, poles[i], unwarp=unwarp)
        gain *= g
    # plane conversions
    if plane == "n":
        zeros /= -2 * np.pi
        f0 = np.abs(zeros)
        f0[f0 <= _epsilon] = 1
        gain *= float(np.prod(2 * np.pi * f0))
        poles /= -2 * np.pi
        f0 = np.abs(poles)
        f0[f0 <= _epsilon] = 1
        gain /= float(np.prod(2 * np.pi * f0))
    elif plane == "f":
        zeros /= 2 * np.pi
        poles /= 2 * np.pi
    elif plane != "s":
        raise ValueError(f"unsupported plane: {plane}")
    zeros = sorted_roots(zeros)
    poles = sorted_roots(poles)
    return (zeros, poles, gain)


def sos2zpk(
    coef: np.ndarray, format: str = "py"
) -> tuple[np.ndarray, np.ndarray, float]:
    """Transforms z-plane second order sections into z-plane roots.

    Poles and zeros at the Nyquist frequency are automatically
    removed.

    If the format is 's' (standard), the order of the coefficients is
    b1, b2, a1, a2. If the format is 'o' (online), the order is
    a1, a2, b1, b2. If the format is 'py', scipy.signal format is used.

    The second order section is defined as:

    `H(z) = (b_0 + b_1 z^{-1} + b_2 z^{-2}) / (1 + a_1 z^{-1} + a_2 z^{-2})`

    Args:
        coef: array of SOS coefficients
        format: how coefficients are specified

    Returns:
        array of z-plane zeros, array of z-plane poles, gain

    """
    if format not in ("s", "o", "py"):
        raise ValueError(f"unsupported format: {format}")
    if format == "py":
        coef = sos_shuffle(coef)
        format = "o"
    if (len(coef) - 1) % 4 != 0:
        raise ValueError(
            f"coef does not have an integral number of stages: " + f"{len(coef)} coeffs"
        )
    zeros = np.array([], dtype=complex)
    poles = np.array([], dtype=complex)
    gain = coef[0]
    soscount = (len(coef) - 1) // 4
    for i in range(soscount):
        if format == "o":
            a1, a2, b1, b2 = coef[4 * i + 1 : 4 * i + 5]
        else:
            b1, b2, a1, a2 = coef[4 * i + 1 : 4 * i + 5]
        order = 2
        if (np.abs(b2) < _epsilon) and (np.abs(a2) < _epsilon):
            order = 1
            if (np.abs(b1) < _epsilon) and (np.abs(a1 < _epsilon)):
                order = 0
                continue
        # first order
        if order == 1:
            # don't count zeros and poles at Nyquist (infinity)
            if np.abs(b1 - 1) > _epsilon:
                zeros = np.append(zeros, -b1)
            if np.abs(a1 - 1) > _epsilon:
                poles = np.append(poles, -a1)
        # second order
        else:
            # don't count zeros at Nyquist (infinity)
            if (np.abs(b1 - 2) < _epsilon) and (np.abs(b2 - 1) < _epsilon):
                # 2 zeros at Nyquist
                pass
            elif np.abs(b1 - b2 - 1) < _epsilon:
                # 1 zero at Nyquist
                zeros = np.append(zeros, -b2)
            else:
                # 0 zeros at Nyquist
                z1, z2 = _quadroots(1, b1, b2)
                zeros = np.append(zeros, [z1, z2])
            if (np.abs(a1 - 2) < _epsilon) and (np.abs(a2 - 1) < _epsilon):
                # 2 poles at Nyquist
                pass
            elif np.abs(a1 - a2 - 1) < _epsilon:
                # 1 pole at Nyquist
                poles = np.append(poles, -a2)
            else:
                # 0 poles at Nyquist
                p1, p2 = _quadroots(1, a1, a2)
                poles = np.append(poles, [p1, p2])
    zeros = sorted_roots(zeros)
    poles = sorted_roots(poles)
    return (zeros, poles, gain)


def zpk2sos(
    zeros: np.ndarray, poles: np.ndarray, gain: float, format: str = "py"
) -> np.ndarray:
    """Transforms z-plane roots into z-plane second order sections.

    Poles and zeros at the Nyquist frequency are automatically added
    as necessary.

    If the format is 's' (standard), the order of the coefficients is
    b1, b2, a1, a2. If the format is 'o' (online), the order is
    a1, a2, b1, b2. If the format is 'py', scipy.signal format is used.

    The second order section is defined as:

    `H(z) = (b_0 + b_1 z^{-1} + b_2 z^{-2}) / (1 + a_1 z^{-1} + a_2 z^{-2})`

    Args:
        zeros: array of z-plane zeros
        poles: array of z-plane poles
        gain: gain
        format: how coefficients are specified

    Returns:
        array of SOS coefficients

    """
    if format not in ("s", "o", "py"):
        raise ValueError(f"unsupported format: {format}")
    order = max(len(zeros), len(poles))
    # add zeros at -1, if necessary
    if len(zeros) < order:
        zeros = np.append(zeros, -np.ones(order - len(zeros)))
    # add poles at f = fSample/pi, if necessary
    if len(poles) < order:
        # if we add two poles, make them Butterworth
        num_pole_pairs = (order - len(poles)) // 2
        p1 = 2 * np.exp(1j * 3 * np.pi / 4)
        p2 = np.conj(p1)
        p1, g1 = bilinear(1, p1)
        p2, g2 = bilinear(1, p2)
        poles = np.append(poles, [p1, p2] * num_pole_pairs)
        gain *= (4 * g1 * g2) ** num_pole_pairs
        # otherwise just one real pole
        if (order - len(poles)) % 2 != 0:
            p, g = bilinear(1, -2)
            poles = np.append(poles, p)
            gain *= -2 * g
    # need to make sure we have complex conjugate pairs
    zeros = sorted_roots(zeros, splane=False)
    poles = sorted_roots(poles, splane=False)
    # now make the SOS
    soscount = (order // 2) + (order % 2)
    coef = np.zeros(4 * soscount + 1, dtype=float)
    coef[0] = gain
    for n in range(order // 2):
        i = 2 * n
        if format == "s":
            coef[4 * n + 1] = -np.real(zeros[i] + zeros[i + 1])
            coef[4 * n + 2] = np.real(zeros[i] * zeros[i + 1])
            coef[4 * n + 3] = -np.real(poles[i] + poles[i + 1])
            coef[4 * n + 4] = np.real(poles[i] * poles[i + 1])
        else:  # 'o' or 'py'
            coef[4 * n + 1] = -np.real(poles[i] + poles[i + 1])
            coef[4 * n + 2] = np.real(poles[i] * poles[i + 1])
            coef[4 * n + 3] = -np.real(zeros[i] + zeros[i + 1])
            coef[4 * n + 4] = np.real(zeros[i] * zeros[i + 1])
    # left over one?
    if order % 2 == 1:
        n = order // 2
        if format == "s":
            coef[4 * n + 1] = -np.real(zeros[-1])
            coef[4 * n + 2] = 0
            coef[4 * n + 3] = -np.real(poles[-1])
            coef[4 * n + 4] = 0
        else:  # 'o' or 'py'
            coef[4 * n + 1] = -np.real(poles[-1])
            coef[4 * n + 2] = 0
            coef[4 * n + 3] = -np.real(zeros[-1])
            coef[4 * n + 4] = 0
    if format == "py":
        coef = sos_unshuffle(coef)
    return coef


# Filter coefficient conversions


def sos_shuffle(coef: np.ndarray) -> np.ndarray:
    """Returns SOS coefficients in advligorts format.

    This function reorganizes the coefficients of a cascaded,
    second order section IIR filter, from the order produced by a
    scipy.signal sos command, to the order used in the advligorts
    real-time C realization (a la Embree). The zeroth coefficient
    is the overall gain factor G, with the remainder of the array
    containing the filter coefficients, in the order:
    [G, a_11, a_21, b_11, b_21, a_12, a_22, b_12, b_22, ... ]
    The b_0i are all unity in this convention.

    """
    # originally implemented for matlab as sos_shuffle.m
    if len(coef.shape) < 2 or coef.shape[1] != 6:
        raise ValueError("coef not in scipy.signal format")
    soses = [coef[n, :] for n in range(coef.shape[0])]
    shuffled_coef = [1]
    for sos in soses:
        shuffled_coef[0] *= sos[0] / sos[3]
        shuffled_coef += [
            sos[4] / sos[3],
            sos[5] / sos[3],
            sos[1] / sos[0],
            sos[2] / sos[0],
        ]
    return np.array(shuffled_coef)


def sos_unshuffle(coef: np.ndarray) -> np.ndarray:
    """Returns SOS coefficients in scipy.signal format.

    Inverse of sos_shuffle.

    """
    if (len(coef) - 1) % 4 != 0:
        raise ValueError(
            "coef does not have an integral number of stages: " + f"{len(coef)} coeffs"
        )
    gain = coef[0]
    soses = []
    for n in range(1, len(coef), 4):
        soses += [np.array([1, coef[n + 2], coef[n + 3], 1, coef[n], coef[n + 1]])]
    if len(soses) == 0:
        # gain-only filter
        soses += [np.array([gain, 0, 0, 1, 0, 0])]
    else:
        # apply overall gain factor to first SOS section
        soses[0][:3] *= gain
    return np.vstack(soses)


def df2biq(coef: np.ndarray) -> np.ndarray:
    """Returns advligorts SOS coefficients converted to biquad format.

    The biquad representation reduces quantization noise by avoiding
    large internal values. The relationship of its coefficients to those
    of the direct form II filter is given in:
    https://dcc.ligo.org/LIGO-G0900928

    """
    # reference implementation:
    # https://git.ligo.org/cds/advligorts/-/blob/master/src/drv/fmReadCoeff.c
    out = np.array(coef)
    for n in range(1, len(coef), 4):
        a1, a2, b1, b2 = coef[n], coef[n + 1], coef[n + 2], coef[n + 3]
        out[n] = -a1 - 1
        out[n + 1] = -a2 - a1 - 1
        out[n + 2] = b1 - a1
        out[n + 3] = b2 - a2 + b1 - a1
    return out


def biqdf2(coef: np.ndarray) -> np.ndarray:
    """Returns advligorts SOS coefficients converted to direct form format.

    Inverse of df2biq.

    """
    out = np.array(coef)
    for n in range(1, len(coef), 4):
        a11, a12, c1, c2 = coef[n], coef[n + 1], coef[n + 2], coef[n + 3]
        out[n] = -a11 - 1
        out[n + 1] = a11 - a12
        out[n + 2] = c1 - a11 - 1
        out[n + 3] = a11 - a12 - c1 + c2
    return out
