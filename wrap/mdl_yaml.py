from typing import IO
import yaml


def dump_yaml(mdl, f: IO[str], prefix: str = "") -> None:
    cfg = dict()

    # parse writable var names from epics db
    epics_db_filename = f"{mdl.module_dir}/{mdl.name}.db"
    with open(epics_db_filename) as db_file:
        epics_db = db_file.readlines()
    var_names = set(
        line.replace("%IFO%", mdl.ifo.upper())
        .replace("%SYS%", mdl.sys.upper())
        .replace("%SUBSYS%", "")
        .split('"')[-2]
        for line in epics_db
        if line.startswith("grecord(ai,") or line.startswith("grecord(bi,")
    )

    # exclude unwanted var names
    var_names -= set(
        name
        for name in var_names
        if name.startswith(f"{mdl.ifo.upper()}:FEC")
        or name.endswith(("REMOTE_IPC_PAUSE", "_SW1S", "_SW2S"))
    )

    var_names = set(mdl._rewrite(chan) for chan in var_names)
    fm_names = set(mdl.filter_modules)
    phase_parts = set(mdl._names["phase_parts"])
    wfs_phase_parts = set(mdl._names["wfs_phase_parts"])

    # fix phase part names
    var_names -= set(f"{x}_E" for x in phase_parts)
    var_names -= set(f"{x}_r" for x in wfs_phase_parts)
    var_names -= set(f"{x}_d" for x in wfs_phase_parts)
    var_names = var_names.union(set(f"{x}" for x in phase_parts))
    var_names = var_names.union(set(f"{x}_R" for x in wfs_phase_parts))
    var_names = var_names.union(set(f"{x}_D" for x in wfs_phase_parts))

    # read vars
    cfg["vars"] = dict((prefix + k, mdl.read(k)) for k in var_names)

    # read fms
    cfg["filters"] = dict()
    for fm_name in fm_names:
        cfg["filters"][prefix + fm_name] = dict()
        cfg["filters"][prefix + fm_name]["switches"] = mdl.fm_get_switches(fm_name)
        for section_name in [f"FM{n+1}" for n in range(10)]:
            sos = mdl.fm_get_sos(fm_name, section_name, format="o").tolist()
            cfg["filters"][prefix + fm_name][section_name] = dict()
            cfg["filters"][prefix + fm_name][section_name]["coef"] = sos
            settings = mdl.fm_get_filter_settings(fm_name, section_name)
            cfg["filters"][prefix + fm_name][section_name]["settings"] = settings

    # write yaml
    yaml.safe_dump(cfg, f, default_flow_style=None)


def load_yaml(mdl, f: IO[str], prefix: str = "") -> None:
    # read yaml
    cfg = yaml.safe_load(f)

    # write vars
    for var, val in cfg["vars"].items():
        mdl.write(prefix + var, val)

    # write fms
    for fm_name in cfg["filters"]:
        mdl.fm_set_switches(prefix + fm_name, **cfg["filters"][fm_name]["switches"])
        for section_name in [f"FM{n+1}" for n in range(10)]:
            sos = cfg["filters"][fm_name][section_name]["coef"]
            mdl.fm_set_sos(prefix + fm_name, section_name, sos, format="o")
            settings = cfg["filters"][fm_name][section_name]["settings"]
            mdl.fm_set_filter_settings(prefix + fm_name, section_name, **settings)
