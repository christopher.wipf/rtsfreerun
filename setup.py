#!/usr/bin/env python3

import os
import sys
import glob
import pathlib
import sysconfig

from setuptools import setup, find_packages, Extension
from setuptools.command import build_ext as build_ext_module

sys.path.append(os.path.dirname(__file__))
import build_freerun

if "model" in os.environ:
    model = os.environ["model"]
else:
    cwd = pathlib.Path.cwd()
    userapps = f"{cwd}/userapps"
    model = glob.glob(f"{userapps}/*.mdl")[0]
    model = pathlib.Path(model).stem

if "module" in os.environ:
    module = os.environ["module"]
else:
    module = model


class build_ext(build_ext_module.build_ext):
    def run(self):
        build_freerun.build(model, module=module, dest_dir=self.build_lib)
        build_ext_module.build_ext.run(self)


setup(
    name=model,
    version="0.2",
    url="https://git.ligo.org/controlsystems/rtsfreerun",
    cmdclass=dict(build_ext=build_ext),
    packages=find_packages(),
    ext_modules=[Extension(name=f"{module}.{model}_pybind", sources=[])],
    package_data={
        "": ["*.txt", "*.db", "*.fmseq", f'*{sysconfig.get_config_var("EXT_SUFFIX")}']
    },
    install_requires=["numpy", "pyyaml"],
    setup_requires=["pybind11"],
    python_requires=">=3.9",
)
