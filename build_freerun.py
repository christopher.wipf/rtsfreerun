#!/usr/bin/env python3

import sys
import os
import pathlib
import shutil
import subprocess
import sysconfig
import atexit
import tempfile


def build(model, module=None, dest_dir=None, advligorts_dir=None, src_dir=None):
    if not dest_dir:
        dest_dir = pathlib.Path.cwd()
    dest_dir = pathlib.Path(dest_dir)

    if not advligorts_dir:
        temp_dir = pathlib.Path(tempfile.mkdtemp())
        atexit.register(shutil.rmtree, temp_dir)
        advligorts_dir = temp_dir / "advligorts"
        shutil.rmtree(advligorts_dir, ignore_errors=True)
        subprocess.run(
            [
                "git",
                "clone",
                "--depth",
                "1",
                "-b",
                "no_zero_pad",
                "https://git.ligo.org/christopher.wipf/advligorts.git",
            ],
            cwd=temp_dir,
            check=True,
        )
    advligorts_dir = pathlib.Path(advligorts_dir)

    src_dir = src_dir or pathlib.Path(__file__).parent
    userapps_dir = src_dir / "userapps"

    if not module:
        module = model
    print(f"building freerunning {model} as package {module}")

    # generate python wrapper
    with (src_dir / "wrap" / "__init__.template").open() as f:
        template = f.read()
    template = template.replace("___MODEL_CLIB_IMPORT___", f"{module}.{model}_pybind")
    template = template.replace("___MODEL___", model)
    (dest_dir / module).mkdir(parents=True, exist_ok=True)
    with (dest_dir / module / "__init__.py").open(mode="w") as f:
        f.write(template)
    shutil.copy(src_dir / "wrap" / "awg.py", dest_dir / module / "awg.py")
    shutil.copy(src_dir / "wrap" / "iirutil.py", dest_dir / module / "iirutil.py")
    shutil.copy(src_dir / "wrap" / "mdl_yaml.py", dest_dir / module / "mdl_yaml.py")

    # generate binary extension module
    (userapps_dir / "ccodeio.h").touch()
    env = os.environ.copy()
    env.setdefault("SITE", "TST")
    env.setdefault("site", "tst")
    env.setdefault("IFO", "X1")
    env.setdefault("ifo", "x1")
    env.setdefault("RCG_LIB_PATH", str(userapps_dir))
    env.setdefault("CDS_SRC", str(userapps_dir))
    env.setdefault("CDS_IFO_SRC", str(userapps_dir))
    env.setdefault("LIBRTS_BINDING_ONLY", "1")
    env.setdefault("USE_STDLIB_MATH", "1")
    env.setdefault(
        "LIBRTS_CMAKE_FLAGS",
        f"-DPYTHON_EXECUTABLE:FILEPATH={sys.executable} -DLIBRTS_NO_EXAMPLES=1 -DLIBRTS_NO_UNIT_TESTS=1 -DUSE_STDLIB_MATH=1",
    )
    if "CIBUILDWHEEL" in env:
        env.setdefault("pythonLocation", str(pathlib.Path(sys.executable).parents[1]))
    subprocess.run(
        ["make", model], cwd=(advligorts_dir / "src" / "librts"), env=env, check=True
    )

    # copy binary extension module
    ext_filename = f'{model}_pybind{sysconfig.get_config_var("EXT_SUFFIX")}'
    shutil.copy(
        advligorts_dir / "src/librts/build/cmake/python/pybind" / ext_filename,
        dest_dir / module / ext_filename,
    )
    # copy filter file, epics db, and fmseq file for inclusion in package data
    shutil.copy(
        advligorts_dir
        / f"src/librts/build/rcg-build/models/{model}/epics/{model}epics-config/{model[:2].upper()}{model.upper()}.txt",
        dest_dir / module / f"{model.upper()}.txt",
    )
    shutil.copy(
        advligorts_dir
        / f"src/librts/build/rcg-build/models/{model}/epics/src/{model}.db",
        dest_dir / module / f"{model}.db",
    )
    shutil.copy(
        advligorts_dir
        / f"src/librts/build/rcg-build/models/{model}/epics/fmseq/{model}",
        dest_dir / module / f"{model}.fmseq",
    )
