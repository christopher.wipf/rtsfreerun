.. default-role:: math

{{ fullname }}
{{ underline }}

.. automodule:: {{ fullname }}
   :members:
